#include "cBullet.h"
#include <vector>

sf::Texture bullet1;
sf::Texture bullet2;

void loadTexture() {
	bullet1.loadFromFile("spaceships/pociskplayer1.png");
	bullet2.loadFromFile("spaceships/pociskplayer2.png");
}

std::vector<cBullet> bullet;
std::vector<cBullet> bulletp2;