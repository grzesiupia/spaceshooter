#include "cBoss.h"
#include "cBossBullet.h"

double velxboss = 0;
double velyboss = 1;

sf::Texture enemyboss;

void loadTexture() {
	enemyboss.loadFromFile("spaceships/boss.png");
}

cBoss bossek(320 - 64, 0 - 128, 1, 1, velxboss, velyboss, enemyboss);