#include "cBackground.h"
#include <iostream>
#include <vector>

sf::Texture background1;
sf::Texture background2;
sf::Texture background3;
sf::Texture background4;
sf::Texture background5;

void loadTexture() {
	background1.loadFromFile("spaceships/background1.png");
	background2.loadFromFile("spaceships/background21.png");
	background3.loadFromFile("spaceships/background31.png");
	background4.loadFromFile("spaceships/background41.png");
	background5.loadFromFile("spaceships/background51.png");
}

cBackground kosmos11(0, 0, 1, 1, 0.1, background1);
cBackground kosmos12(0, -960, 1, 1, 0.1, background1);
cBackground kosmos21(0, 0, 1, 1, 0.2, background2);
cBackground kosmos22(0, -960, 1, 1, 0.2, background2);
cBackground kosmos31(0, 0, 1, 1, 0.3, background3);
cBackground kosmos32(0, -960, 1, 1, 0.3, background3);
cBackground kosmos41(0, 0, 1, 1, 0.4, background4);
cBackground kosmos42(0, -960, 1, 1, 0.4, background4);
cBackground kosmos51(0, 0, 1, 1, 0.5, background5);
cBackground kosmos52(0, -960, 1, 1, 0.5, background5);

std::vector<cBackground> tlo1;
std::vector<cBackground> tlo2;
std::vector<cBackground> tlo3;
std::vector<cBackground> tlo4;
std::vector<cBackground> tlo5;