#include "cPlayer.h"
#include "SFML/Audio.hpp"

sf::SoundBuffer buffer;

sf::Texture player1;
sf::Texture player2;

void loadTexture() {
	player1.loadFromFile("spaceships/player1.png");
	player2.loadFromFile("spaceships/player2.png");
	buffer.loadFromFile("sounds/strzal.wav");
}

cPlayer gracz(320 - 32, 960 - (64 + 160), 1, 1, player1);
cPlayer gracz2(320 - 32, 960 - (64 + 160), 1, 1, player2);
