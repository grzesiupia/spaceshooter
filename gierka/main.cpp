#include <SFML/Graphics.hpp>
#include <SFML/Window.hpp>
#include <SFML/System.hpp>
#include <SFML/Audio.hpp>

#include <iostream>
#include <cstdlib>
#include <ctime>

#include "cBoss.h"
#include "cBackground.h"
#include "cBullet.h"
#include "cPlayer.h"
#include "cUi.h"
#include "cBoost.h"
#include "cBossBullet.h"
#include "cEnemyBullet.h"
#include "cEnemy.h"

sf::Uint32 klatki = 0;
int zycie = 3;
int scena = 1;
int szybkoscstrzelania = 200;
int prawdopodobienstwobusta = 10000;
int punkty = 0;
std::string spunkty = std::to_string(punkty);

double zyckobossa = 20;


int main()
{

#pragma region Dzwieki

	sf::Music muzykamenu;
	muzykamenu.openFromFile("sounds/muzykamenu.ogg");

#pragma endregion


	sf::RenderWindow window(sf::VideoMode(640, 960), "STAR WARS - RETURN OF THE GRZESIU");
	sf::Clock Clock;
	window.setFramerateLimit(500);
	tlo1.push_back(kosmos11);
	tlo2.push_back(kosmos21);
	tlo3.push_back(kosmos31);
	tlo4.push_back(kosmos41);
	tlo5.push_back(kosmos51);


	while (window.isOpen())
	{


		sf::Event e;
		while (window.pollEvent(e))
		{
			if (e.type == sf::Event::Closed)
				window.close();
			if (e.type == sf::Event::Resized)
				window.setSize(sf::Vector2u(640, 960));
		}

		gracz.control(scena);

		muzykamenu.setLoop(true);

		if (scena == 1)
		{
			muzykamenu.setVolume(2);
			muzykamenu.play();
		}

		if (scena == 1)
		{
			if (sf::Keyboard::isKeyPressed(sf::Keyboard::Down) && (podkreslenie.y_ <= 530))
			{
				podkreslenie.y_ += 150;
			}
			if (sf::Keyboard::isKeyPressed(sf::Keyboard::Up) && (podkreslenie.y_ >= 680))
			{
				podkreslenie.y_ -= 150;
			}
			if (sf::Keyboard::isKeyPressed(sf::Keyboard::Return) && podkreslenie.y_ == 530)
			{
				scena = 2;
				klatki = 0;
			}
			if (sf::Keyboard::isKeyPressed(sf::Keyboard::Return) && podkreslenie.y_ == 680)
			{
				break;
			}
		}
		if (scena == 4)
		{
			if (sf::Keyboard::isKeyPressed(sf::Keyboard::Return))
			{
				break;
			}
		}
		if (scena == 3)
		{

		}



		if (scena == 1 || scena == 2 || scena == 3 || scena == 4)
		{

			///NIESKONCZONE TLO
			//TLO NR1
			if (tlo1.back().y_ >= 0 && tlo1.back().y_ <= 1)
			{
				tlo1.push_back(kosmos12);
			}

			//TLO NR2
			if (tlo2.back().y_ >= 0 && tlo2.back().y_ <= 1)
			{
				tlo2.push_back(kosmos22);
			}

			//TLO NR3
			if (tlo3.back().y_ >= 0 && tlo3.back().y_ <= 1)
			{
				tlo3.push_back(kosmos32);
			}

			//TLO NR4
			if (tlo4.back().y_ >= 0 && tlo4.back().y_ <= 1)
			{
				tlo4.push_back(kosmos42);
			}

			//TLO NR5
			if (tlo5.back().y_ == 0)
			{
				tlo5.push_back(kosmos52);
			}

		}

		if (scena == 2)
		{
			///PRZECIWNICY///

			if (klatki >= 2000 && klatki <= 20000 || klatki >= 28000 && klatki <= 34000)
			{
				if (klatki % 150 == 0)
				{
					sonda.x_ = rand() % (640 - 64);
					sondy.push_back(sonda);

				}
			}

			if (klatki >= 22000 && klatki <= 26000 || klatki >= 30000 && klatki <= 34000)
			{
				if (klatki % 400 == 0)
				{
					enemy.x_ = 0; enemy.vel_x_ = 1;
					przeciwnik.push_back(enemy);

				}
				if (klatki % 400 == 0)
				{
					enemy.x_ = (640 - 64); enemy.vel_x_ = -1;
					przeciwnik2.push_back(enemy);
				}
			}

			///BOOSTY///

			if (szybkoscstrzelania >= 50)
			{
				if (rand() % prawdopodobienstwobusta == 0)
				{
					buststrzelania.x_ = rand() % (640 - 24);
					booststrzal.push_back(buststrzelania);
				}
			}

			///BOSS///
			if (bossek.y_ == 64 && bossek.vel_x_ == 0)
			{
				bossek.vel_y_ = 0;
				bossek.vel_x_ = 0.5;
			}
			if (bossek.vel_x_ == 0.5 && bossek.x_ == (640 - 128))
			{
				bossek.vel_x_ = -0.5;
			}
			if (bossek.vel_x_ == -0.5&&bossek.x_ == 0)
			{
				bossek.vel_x_ = 0.5;
			}

		}
		if (scena == 2 || scena == 3)
		{
			///STRZELANIE
			//GRACZA1
			if (klatki%szybkoscstrzelania == 0)
			{
				gracz.shoot(bullet, bullet1);
			}

			//GRACZA2
			if (klatki%szybkoscstrzelania == 0)
			{
				gracz2.shoot(bulletp2, bullet2);
			}

			//PRZECIWNIKOW
			if (klatki % 200 == 0)
			{
				for (int i = 0; i < przeciwnik.size(); i++)
				{
					przeciwnik[i].shoot(ebullet, bulletenemy1);
				}
			}

			//BOSSA
			if (klatki % 200 == 0 && bossek.y_ == 64)
			{
				bossek.shoot(bbullet1, bulletenemy1);
				bossek.shoot(bbullet2, bulletenemy1);
				bossek.shoot(bbullet3, bulletenemy1);
				for (int i = 0; i < bbullet1.size(); i++)
				{
					bbullet1[i].vel_x_ = 0.5;
				}
				for (int i = 0; i < bbullet2.size(); i++)
				{
					bbullet2[i].vel_x_ = -0.5;
				}
				for (int i = 0; i < bbullet3.size(); i++)
				{
					bbullet3[i].vel_x_ = 0;
				}
			}

		}


		window.clear(sf::Color::White);

#pragma region Wyswietlanie

		//POKAZYWANIE TLA1
		for (int i = 0; i < tlo1.size(); i++)
		{
			tlo1[i].update(window);
		}
		//TLO2
		for (int i = 0; i < tlo2.size(); i++)
		{
			tlo2[i].update(window);
		}
		//TLO3
		for (int i = 0; i < tlo3.size(); i++)
		{
			tlo3[i].update(window);
		}
		//TLO4
		for (int i = 0; i < tlo4.size(); i++)
		{
			tlo4[i].update(window);
		}
		//TLO5
		for (int i = 0; i < tlo5.size(); i++)
		{
			tlo5[i].update(window);
		}

		//POKAZYWANIE MENU
		if (scena == 1)
		{
			starcik.update(window);
			wyjscie.update(window);
			podkreslenie.update(window);
		}

		//POKAZYWANIE POCISKOW
		for (auto itr = bullet.begin(); itr != bullet.end(); itr++)
		{
			itr->update(window);
		}

		//POKAZYWANIE GRACZA
		if (scena == 2 || scena == 3)
		{
			gracz.update(window);
		}

		//POKAZYWANIE SIE BOSSA
		if ((scena == 2) && klatki >= 36000)
		{
			bossek.update(window);
		}

		//POKAZYWANIE SIE PRZECIWNIKOW

		for (int i = 0; i < sondy.size(); i++)
		{
			sondy.at(i).update(window);

		}

		for (int i = 0; i < przeciwnik.size(); i++)
		{
			przeciwnik.at(i).update(window);

		}

		for (int i = 0; i < przeciwnik2.size(); i++)
		{
			przeciwnik2.at(i).update(window);

		}

		//POKAZYWANIE SIE POCISKOW PRZECIWNIKOW
		for (auto itr = ebullet.begin(); itr != ebullet.end(); itr++)
		{
			itr->update(window);
		}

		//POKAZYWANIE SIE POCISKOW BOSSA

		if (scena == 2)
		{
			for (auto itr = bbullet1.begin(); itr != bbullet1.end(); itr++)
			{
				itr->update(window);
			}
			for (auto itr = bbullet2.begin(); itr != bbullet2.end(); itr++)
			{
				itr->update(window);
			}
			for (auto itr = bbullet3.begin(); itr != bbullet3.end(); itr++)
			{
				itr->update(window);
			}
		}

		//POKAZYWANIE BOOSTOW
		if (scena == 2 || scena == 3)
		{
			for (int i = 0; i < booststrzal.size(); i++)
			{
				booststrzal.at(i).update(window);

			}
		}

		if (scena == 2 || scena == 3)
		{
			//POKAZYWANIE SERDUSZEK
			if (zycie >= 1)
			{
				heart1.update(window);
			}
			if (zycie >= 2)
			{
				heart1.update(window);
				heart2.update(window);
			}
			if (zycie >= 3)
			{
				heart1.update(window);
				heart2.update(window);
				heart3.update(window);
			}
		}
		if (bossek.y_ == 64)
		{
			bosslife.update(window);
		}
		//

#pragma endregion

#pragma region Kolizje

		//GRACZ VS PRZECIWNIK

		for (int i = 0; i < przeciwnik.size(); i++)
		{
			if (gracz.x_ + 64 >= przeciwnik[i].x_&&gracz.x_ <= przeciwnik[i].x_ + 64 && gracz.y_ >= przeciwnik[i].y_ - 64 && gracz.y_ - 64 <= przeciwnik[i].y_)
			{
				
				przeciwnik.erase(przeciwnik.begin() + i);
				szybkoscstrzelania = 200;
				zycie--;
				break;
			}

		}

		for (int i = 0; i < przeciwnik2.size(); i++)
		{
			if (gracz.x_ + 64 >= przeciwnik2[i].x_&&gracz.x_ <= przeciwnik2[i].x_ + 64 && gracz.y_ >= przeciwnik2[i].y_ - 64 && gracz.y_ - 64 <= przeciwnik2[i].y_)
			{
				
				przeciwnik2.erase(przeciwnik2.begin() + i);
				szybkoscstrzelania = 200;
				zycie--;
				break;
			}

		}

		for (int i = 0; i < sondy.size(); i++)
		{
			if (gracz.x_ + 64 >= sondy[i].x_&&gracz.x_ <= sondy[i].x_ + 64 && gracz.y_ >= sondy[i].y_ - 64 && gracz.y_ - 64 <= sondy[i].y_)
			{
				
				sondy.erase(sondy.begin() + i);
				szybkoscstrzelania = 200;
				zycie--;

				break;
			}

		}

		if (gracz.x_ + 64 >= bossek.x_&&gracz.x_ <= bossek.x_ + 128 && gracz.y_ >= bossek.y_ - 128 && gracz.y_ - 64 <= bossek.y_)
		{
			
			szybkoscstrzelania += 50;
			zycie--;
			break;
		}

		//GRACZ VS BOOST
		for (int i = 0; i < booststrzal.size(); i++)
		{
			if (gracz.x_ + 64 >= booststrzal[i].x_&&gracz.x_ <= booststrzal[i].x_ + 24 && gracz.y_ >= booststrzal[i].y_ - 24 && gracz.y_ - 64 <= booststrzal[i].y_)
			{
				booststrzal.erase(booststrzal.begin() + i);
				szybkoscstrzelania -= 50;
				if (szybkoscstrzelania == 50)
					szybkoscstrzelania -= 30;
				prawdopodobienstwobusta += 2000;
				break;
			}

		}
		//POCISKI GRACZA VS PRZECIWNIK

		for (int i = 0; i < przeciwnik.size(); i++)
		{

			for (int j = 0; j < bullet.size(); j++)
			{
				if (bullet[j].x_ <= przeciwnik[i].x_ + 64 && bullet[j].x_ + 8 >= przeciwnik[i].x_ && bullet[j].y_ <= przeciwnik[i].y_ + 64 && bullet[j].y_ + 16 >= przeciwnik[i].y_)
				{
					przeciwnik.erase(przeciwnik.begin() + i);
					bullet.erase(bullet.begin() + j);
					punkty += 100;
				
					break;
				}
			}
		}

		for (int i = 0; i < przeciwnik2.size(); i++)
		{

			for (int j = 0; j < bullet.size(); j++)
			{
				if (bullet[j].x_ <= przeciwnik2[i].x_ + 64 && bullet[j].x_ + 8 >= przeciwnik2[i].x_ && bullet[j].y_ <= przeciwnik2[i].y_ + 64 && bullet[j].y_ + 16 >= przeciwnik2[i].y_)
				{
					przeciwnik2.erase(przeciwnik2.begin() + i);
					bullet.erase(bullet.begin() + j);
					punkty += 100;
					
					break;
				}
			}
		}

		for (int i = 0; i < sondy.size(); i++)
		{

			for (int j = 0; j < bullet.size(); j++)
			{
				if (bullet[j].x_ <= sondy[i].x_ + 64 && bullet[j].x_ + 8 >= sondy[i].x_ && bullet[j].y_ <= sondy[i].y_ + 64 && bullet[j].y_ + 16 >= sondy[i].y_)
				{
					sondy.erase(sondy.begin() + i);
					bullet.erase(bullet.begin() + j);
					punkty += 50;
					
					break;
				}
			}
		}


		///BOSS
		for (int j = 0; j < bullet.size(); j++)
		{
			if (bullet[j].x_ <= bossek.x_ + 128 && bullet[j].x_ + 8 >= bossek.x_ && bullet[j].y_ <= bossek.y_ + 128 && bullet[j].y_ + 16 >= bossek.y_)
			{
				bullet.erase(bullet.begin() + j);
				punkty += 50;
				bosslife.width_ -= 0.05;
				zyckobossa -= 1;
				
				break;
			}
		}

		//POCISKI PRZECIWNIKA VS GRACZ

		for (int i = 0; i < ebullet.size(); i++)
		{
			if (gracz.x_ + 64 >= ebullet[i].x_&&gracz.x_ <= ebullet[i].x_ + 16 && gracz.y_ >= ebullet[i].y_ - 16 && gracz.y_ - 64 <= ebullet[i].y_)
			{
				
				ebullet.erase(ebullet.begin() + i);
				zycie--;
				szybkoscstrzelania += 50;
				break;
			}
		}

		///BOSSA
		for (int i = 0; i < bbullet1.size(); i++)
		{
			if (gracz.x_ + 64 >= bbullet1[i].x_&&gracz.x_ <= bbullet1[i].x_ + 16 && gracz.y_ >= bbullet1[i].y_ - 16 && gracz.y_ - 64 <= bbullet1[i].y_)
			{
				
				bbullet1.erase(bbullet1.begin() + i);
				zycie--;
				szybkoscstrzelania += 50;
				break;
			}
		}
		for (int i = 0; i < bbullet2.size(); i++)
		{
			if (gracz.x_ + 64 >= bbullet2[i].x_&&gracz.x_ <= bbullet2[i].x_ + 16 && gracz.y_ >= bbullet2[i].y_ - 16 && gracz.y_ - 64 <= bbullet2[i].y_)
			{
				
				bbullet2.erase(bbullet2.begin() + i);
				zycie--;
				szybkoscstrzelania += 50;
				break;
			}
		}
		for (int i = 0; i < bbullet3.size(); i++)
		{
			if (gracz.x_ + 64 >= bbullet3[i].x_&&gracz.x_ <= bbullet3[i].x_ + 16 && gracz.y_ >= bbullet3[i].y_ - 16 && gracz.y_ - 64 <= bbullet3[i].y_)
			{
				
				bbullet3.erase(bbullet3.begin() + i);
				zycie--;
				szybkoscstrzelania += 50;
				break;
			}
		}


#pragma endregion

#pragma region Usuwanie

		//USUWANIE PRZECIWNIKOW
		for (int i = 0; i < przeciwnik.size(); i++)
		{
			if (przeciwnik.at(i).aktywny == false)
			{
				przeciwnik.erase(przeciwnik.begin() + i);

				i--;
			}
		}

		for (int i = 0; i < sondy.size(); i++)
		{
			if (sondy.at(i).aktywny == false)
			{
				sondy.erase(sondy.begin() + i);
				break;
			}
		}

		//USUWANIE POCISKOW
		for (int i = 0; i < bullet.size(); i++)
		{
			if (bullet.at(i).aktywny == false)
			{
				bullet.erase(bullet.begin() + i);
				break;
			}
		}

		//USUWANIE POCISKOW PRZECIWNIKOW
		for (int i = 0; i < ebullet.size(); i++)
		{
			if (ebullet.at(i).aktywny == false)
			{
				ebullet.erase(ebullet.begin() + i);
				break;
			}
		}

		//USUWANIE BOOSTOW
		for (int i = 0; i < booststrzal.size(); i++)
		{
			if (booststrzal.at(i).aktywny == false)
			{
				booststrzal.erase(booststrzal.begin() + i);
				break;
			}
		}

		//USUWANIE TLA1
		for (int i = 0; i < tlo1.size(); i++)
		{
			if (tlo1.at(i).aktywny == false)
			{
				tlo1.erase(tlo1.begin() + i);
				break;
			}
		}

		//USUWANIE TLA2
		for (int i = 0; i < tlo2.size(); i++)
		{
			if (tlo2.at(i).aktywny == false)
			{
				tlo2.erase(tlo2.begin() + i);
				break;
			}
		}

		//USUWANIE TLA3
		for (int i = 0; i < tlo3.size(); i++)
		{
			if (tlo3.at(i).aktywny == false)
			{
				tlo3.erase(tlo3.begin() + i);
				break;
			}
		}

		//USUWANIE TLA4
		for (int i = 0; i < tlo4.size(); i++)
		{
			if (tlo4.at(i).aktywny == false)
			{
				tlo4.erase(tlo4.begin() + i);
				break;
			}
		}

		//USUWANIE TLA5
		for (int i = 0; i < tlo5.size(); i++)
		{
			if (tlo5.at(i).aktywny == false)
			{
				tlo5.erase(tlo5.begin() + i);
				break;
			}
		}

#pragma endregion

		//GAME OVER//
		if (zycie <= 0)
		{
			scena = 4;
			gameover.update(window);
		}

		if (zyckobossa == 0)
		{
			scena = 3;
		}


		window.display();
		klatki++;
	}
	return 0;
}