#pragma once

#include "SFML/Graphics.hpp"
#include <iostream>

class cBackground
{
public:
	double x_;
	double y_;

	double velocity_;

	double width_;
	double hight_;

	bool aktywny = true;

	sf::Texture background_;

	cBackground(double x, double y, double width, double hight, double velocity, sf::Texture background)
	{
		x_ = x;
		y_ = y;
		width_ = width;
		hight_ = hight;
		background_ = background;
		velocity_ = velocity;
	}

	void update(sf::RenderWindow &window)
	{
		y_ += velocity_;
		sf::Sprite sprajt(background_);
		sprajt.setScale(width_, hight_);
		sprajt.setPosition(x_, y_);
		window.draw(sprajt);
		if (y_ > 960)
			aktywny = false;
	}
};