#pragma once
#include "SFML/Graphics.hpp"
#include "cBossBullet.h"
#include <vector>
#include <iostream>

class cBoss
{
public:
	double x_;
	double y_;

	sf::Texture boss_;

	bool aktywny = true;

	double vel_x_;
	double vel_y_;

	double width_;
	double hight_;

	cBoss(double x, double y, double width, double hight, double velx, double vely, sf::Texture boss)
	{
		x_ = x;
		y_ = y;
		width_ = width;
		hight_ = hight;
		boss_ = boss;
		vel_x_ = velx;
		vel_y_ = vely;
	}

	void update(sf::RenderWindow &window)
	{

		y_ += vel_y_;
		x_ += vel_x_;

		sf::Sprite sprajt(boss_);
		sprajt.setPosition(x_, y_);
		sprajt.setScale(width_, hight_);
		window.draw(sprajt);
		if (y_ > 960)
			aktywny = false;
	}

	void shoot(std::vector<cBBullet> &bullets, sf::Texture tekstura)
	{
		cBBullet bullet(x_ + 56, y_ + 128, 2, 2, 1, 1, tekstura);
		bullets.push_back(bullet);
	}

};