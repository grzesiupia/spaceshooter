#pragma once
#include <SFML/Graphics.hpp>
#include <iostream>
#include <vector>
#include <ctime>



class cUI
{
public:
	double x_;
	double y_;

	double width_;
	double hight_;

	bool aktywny = true;

	sf::Texture tekstura_;

	cUI(double x, double y, double width, double hight, sf::Texture tekstura)
	{
		x_ = x;
		y_ = y;
		width_ = width;
		hight_ = hight;
		tekstura_ = tekstura;
	}

	void update(sf::RenderWindow &window)
	{
		sf::Sprite sprajt(tekstura_);
		sprajt.setScale(width_, hight_);
		sprajt.setPosition(x_, y_);
		window.draw(sprajt);
	}

};