#include "cUi.h"
#include <vector>

sf::Texture serduszko;
sf::Texture przegrales;
sf::Texture start;
sf::Texture exit;
sf::Texture zyciebossa;
sf::Texture tile;

void loadTextures() {
	serduszko.loadFromFile("spaceships/serce.png");
	przegrales.loadFromFile("spaceships/gameover.png");
	start.loadFromFile("spaceships/start.png");
	exit.loadFromFile("spaceships/exit.png");
	zyciebossa.loadFromFile("spaceships/zyciebossa.png");
	tile.loadFromFile("spaceships/tile.png");
}

cUI heart1(5, 960 - 58, 1, 1, serduszko);
cUI heart2(5 + 60, 960 - 58, 1, 1, serduszko);
cUI heart3(5 + 120, 960 - 58, 1, 1, serduszko);
cUI gameover(0 + 5, 0 + 295, 1, 1, przegrales);
cUI starcik(84, 400, 0.75, 0.75, start);
cUI wyjscie(84, 550, 0.75, 0.75, exit);
cUI podkreslenie(0, 530, 1, 1, tile);
cUI bosslife(0, 10, 1, 1, zyciebossa);

std::vector<cUI> serduszka;