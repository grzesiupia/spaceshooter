#pragma once
#include "SFML/Graphics.hpp"
#include "cEnemyBullet.h"
#include <vector>
#include <iostream>

class cEnemy
{
public:
	double x_;
	double y_;

	sf::Texture enemy_;

	bool aktywny = true;

	double vel_x_;
	double vel_y_;

	double width_;
	double hight_;

	cEnemy(double x, double y, double width, double hight, double velx, double vely, sf::Texture enemy)
	{
		x_ = x;
		y_ = y;
		width_ = width;
		hight_ = hight;
		enemy_ = enemy;
		vel_x_ = velx;
		vel_y_ = vely;
	}

	void update(sf::RenderWindow &window)
	{

		y_ += vel_y_;
		x_ += vel_x_;

		sf::Sprite sprajt(enemy_);
		sprajt.setPosition(x_, y_);
		sprajt.setScale(width_, hight_);
		window.draw(sprajt);
		if (y_ > 960 + 64)
			aktywny = false;
		if (x_ > 640)
			aktywny = false;
		if (x_ < -64)
			aktywny = false;
	}

	void shoot(std::vector<cEBullet> &bullets, sf::Texture tekstura)
	{
		if (rand() % 2 == 0)
		{
			cEBullet bullet(x_ + 30, y_, 2, 2, tekstura);
			bullets.push_back(bullet);
		}
	}
};