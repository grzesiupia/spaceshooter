#pragma once

#include "SFML/Graphics.hpp"
#include <iostream>

class cBoost
{
	friend class cEnemy;

public:
	double x_;
	double y_;

	sf::Texture boost_;

	double width_;
	double hight_;

	bool aktywny = true;

	double velocity_ = 1;

	cBoost(double x, double y, double width, double hight, sf::Texture boost)
	{
		x_ = x;
		y_ = y;
		width_ = width;
		hight_ = hight;
		boost_ = boost;
	}

	void update(sf::RenderWindow &window)
	{
		y_ += velocity_;
		sf::Sprite sprajt(boost_);
		sprajt.setPosition(x_, y_);
		sprajt.setScale(width_, hight_);
		window.draw(sprajt);
		if (y_ > 960)
		{
			aktywny = false;
		}
	}

};