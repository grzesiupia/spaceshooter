#pragma once

#include "SFML/Graphics.hpp"
#include <iostream>

class cBullet
{
	friend class cPlayer;

public:
	double x_;
	double y_;

	double velocity_ = 2;

	sf::Texture bullet_;

	bool aktywny = true;

	double width_;
	double hight_;

	double damage_ = 2;

	cBullet(double x, double y, double width, double hight, sf::Texture bullet)
	{
		x_ = x;
		y_ = y;
		width_ = width;
		hight_ = hight;
		bullet_ = bullet;
	}

	void update(sf::RenderWindow &window)
	{
		y_ -= velocity_;
		sf::Sprite sprajt(bullet_);
		sprajt.setScale(width_, hight_);
		sprajt.setPosition(x_, y_);
		window.draw(sprajt);
		if (y_ < 16)
			aktywny = false;
	}
};