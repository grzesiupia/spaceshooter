#include "cEnemy.h"

sf::Texture enemy1;
sf::Texture enemy2;

void loadTexture() {
	enemy1.loadFromFile("spaceships/enemy1.png");
	enemy2.loadFromFile("spaceships/sonda.png");
}

cEnemy enemy(0, -64, 1, 1, 1, 1, enemy1);
cEnemy sonda(0, -64, 1, 1, 0, 0.5, enemy2);

std::vector<cEnemy> przeciwnik;
std::vector<cEnemy> przeciwnik2;
std::vector<cEnemy> sondy;